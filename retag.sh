#!/bin/bash

docker images --format '{{.Repository}}:{{.Tag}}' | grep '^panther.XXXX/' | while read -r image; do
    name_and_tag="${image#panther.XXXX/}"
    new_image="centralrepoXXX/XYZ/${name_and_tag}"
    echo docker tag "$image" "$new_image"
done

# Start from the latest Golang base image
FROM golang:latest as builder

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy the go source files
COPY . .

# Get the dependencies (if any)
RUN go get -d -v

# Build the Go app
RUN CGO_ENABLED=0 GOOS=linux go build -o extract-names .

### Start a new stage from scratch ###
FROM alpine:latest

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /app/extract-names /app/

# Set the Current Working Directory inside the container
WORKDIR /app

# Command to run the executable
ENTRYPOINT ["./extract-names"]

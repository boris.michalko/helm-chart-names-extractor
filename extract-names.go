package main

import (
	"fmt"
	"os"
    "io"
	"path/filepath"
	"strings"

	"gopkg.in/yaml.v3"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: extract-names <helm_template_generated_directory>")
		os.Exit(1)
	}

	helmDir := os.Args[1]
	result := make(map[string]interface{})

	err := filepath.Walk(helmDir, func(path string, info os.FileInfo, err error) error {
        if err != nil {
            return err
        }
        if !strings.HasSuffix(info.Name(), ".yaml") && !strings.HasSuffix(info.Name(), ".yml") {
            return nil
        }
        fmt.Println(path)

        content, err := os.ReadFile(path)
        if err != nil {
            return err
        }

        decoder := yaml.NewDecoder(strings.NewReader(string(content)))

        for {
            var doc interface{}
            err = decoder.Decode(&doc)
            if err == io.EOF { // Exit the loop when all documents have been read
                break
            }
            if err != nil {
                return err
            }

            switch t := doc.(type) {
            case map[string]interface{}:
                extractNames(t, path, "", result)
            default:
                return fmt.Errorf("unexpected type: %T", doc)
            }
        }
        return nil
    })

	if err != nil {
		fmt.Printf("Error processing files: %v\n", err)
		os.Exit(1)
	}

	outputYAML, err := yaml.Marshal(result)
	if err != nil {
		fmt.Printf("Error marshaling output: %v\n", err)
		os.Exit(1)
	}

	err = os.WriteFile("name-values.yaml", outputYAML, 0644)
	if err != nil {
		fmt.Printf("Error writing output file: %v\n", err)
		os.Exit(1)
	}
}

func extractNames(obj interface{}, path string, currentKey string, result map[string]interface{}) {
	switch o := obj.(type) {
	case map[string]interface{}:
		for k, v := range o {
			newKey := k
			if currentKey != "" {
				newKey = currentKey + "." + k
			}
			extractNames(v, path, newKey, result)
		}
	case []interface{}:
		for i, v := range o {
			newKey := fmt.Sprintf("%s[%d]", currentKey, i)
			extractNames(v, path, newKey, result)
		}
	default:
		if strings.HasSuffix(currentKey, "name") {
			result[fmt.Sprintf("%s.%s", path, currentKey)] = o
		}
	}
}
